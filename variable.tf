variable "aws_region" {

  type    = string
  default = "us-east-2"
}

/* network module variables*/


variable "environment" {

  type    = string
  default = "production"
}

variable "cidr" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.8.0.0/16"
}

variable "public_subnets" {

  description = "A map of availability zones to public cidrs"
  type        = map(string)
  default = {

    us-east-2a = "10.8.16.0/20",
    us-east-2b = "10.8.32.0/20",
    us-east-2c = "10.8.48.0/20"

  }
}

variable "enable_dns_hostnames" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}


variable "enable_dns_support" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "private_subnets_app" {
  description = "A map of availability zones to private cidrs"
  type        = map(string)
  default = {

    us-east-2a = "10.8.64.0/20",
    us-east-2b = "10.8.80.0/20",
    us-east-2c = "10.8.96.0/20"

  }
}


variable "private_subnets_db" {
  description = "A map of availability zones to private cidrs"
  type        = map(string)
  default = {

    us-east-2a = "10.8.112.0/20",
    us-east-2b = "10.8.128.0/20",
    us-east-2c = "10.8.144.0/20"

  }
}


variable "cluster_name" {

  type    = string
  default = "eks-cluster"

}
