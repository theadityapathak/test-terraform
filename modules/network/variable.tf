
variable "environment" {}

variable "cidr" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.16.0.0/16"
}

variable "public_subnets" {
  description = "A map of availability zones to public cidrs"
  type        = map(string)
  default = {

    us-east-2a = "",
    us-east-2b = "",
    us-east-2c = ""

  }
}


variable "enable_dns_hostnames" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}

variable "enable_dns_support" {
  description = "Should be true if you want to use private DNS within the VPC"
  default     = true
}

/*variable "public_propagating_vgws" {
  description = "A list of VGWs the public route table should propagate."
  default     = []
}*/



variable "private_subnets_app" {
  description = "A map of availability zones to private cidrs"
  type        = map(string)
  default = {

    us-east-1a = "",
    us-east-1b = "",
    us-east-1c = ""

  }
}


variable "private_subnets_db" {
  description = "A map of availability zones to private cidrs"
  type        = map(string)
  default = {

    us-east-1a = "",
    us-east-1b = "",
    us-east-1c = ""

  }
}

/*variable "private_propagating_vgws" {
  description = "A list of VGWs the private route table should propagate."
  default     = []
}*/

variable "cluster_name" {
  type    = string
  default = ""
}